# timber-tac-toe 

This infuriating game pits a human against an unbeatable computer in a game of timber-tac-toe

## Getting Started

1. Clone the repository to your local machine
2. Update Line 6 in server.js to point to your local directory
3. Run `npm install`
4. Run `webpack`
5. Navigate to localhost:3000
6. Lose to the AI
 
### Prerequisites

You will need javascript, node and webpack to run this project

