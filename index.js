import { createStore } from 'redux';
import * as PIXI from 'pixi.js'
import moveReducer from './src/Reducer/MoveReducer'
import moveAction from './src/Action/MoveAction'
import { isGameOver } from './src/Board/Board'
import { calculateComputerMove } from './src/Board/AI'

let store = createStore(moveReducer)

const app = new PIXI.Application();
document.body.appendChild(app.view);

const blankSquareTexture = PIXI.Texture.from('img/blank-square.png')
const xSquareTexture = PIXI.Texture.from('img/x-square.png')
const oSquareTexture = PIXI.Texture.from('img/o-square.png')
const squares = [[], [], []];

createBoard()

/**
 *
 * @param player
 * @param square
 * @param texture
 */
function makeMove(player, square, texture) {
  store.dispatch(moveAction(player, square))

  square.texture = texture
  isGameOver(store.getState())
}

function handlePlayerMove() {
  if(this.player === 'empty') {
    this.texture = xSquareTexture

    makeMove('x', this, xSquareTexture)

    const move = calculateComputerMove(store.getState())
    makeMove('o', squares[move[0]][move[1]], oSquareTexture)
  }
}

function createBoard() {
  const widthUnit = app.screen.width/3
  const heightUnit = app.screen.height/3

  const xPositions = [
    0, widthUnit, widthUnit * 2
  ]

  const yPositions = [
    0, heightUnit, heightUnit * 2
  ]

  for (let x = 0; x < 3; x++) {
    for (let y = 0; y < 3; y++) {
      const square = new PIXI.Sprite(blankSquareTexture);

      square.interactive = true
      square.buttonMode = true;
      square.x = xPositions[x] + 5
      square.y = yPositions[y] + 5
      square.width = widthUnit - 10
      square.height = heightUnit - 10
      square.xIndex = x
      square.yIndex = y
      square.player = 'empty'

      square.on('pointertap', handlePlayerMove)

      app.stage.addChild(square)

      squares[x][y] = square
    }
  }
}




