export default function moveReducer(
  state=[['empty', 'empty', 'empty'],
         ['empty', 'empty', 'empty'],
         ['empty', 'empty', 'empty']],
  action) {
  let newState = [...state]

  switch(action.type)
  {
    case 'MOVE':
      newState[action.x][action.y] = action.player
      return newState
    default:
      return state
  }
}