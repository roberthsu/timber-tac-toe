import {hasDraw, hasWinner, isGameOver} from './Board'

export function calculateComputerMove(currentBoard) {
  const board = copyBoard(currentBoard)
  const computerValidMoves = getValidMoves(board)

  const winningMoves = computerValidMoves.map((simulatedComputerMove) => {
    let tempComputerBoard = copyBoard(board)

    tempComputerBoard[simulatedComputerMove[0]][simulatedComputerMove[1]] = 'o'

    if(hasWinner(tempComputerBoard, 'o') || hasDraw(tempComputerBoard)) {
      return simulatedComputerMove
    }

    return false
  }).filter((item) => {
    return item
  })

  if(winningMoves.length) {
    return winningMoves[0]
  } else {
    const safeMoves = computerValidMoves.map((move) => {
      if(isSafe(board, move)) {
        return move
      }
    }).filter((item) => {
      return item
    })

    return safeMoves[0]
  }
}

/**
 *
 * @param board
 * @returns {Array}
 */
function getValidMoves(board)
{
  let validMoves = []
  for (let x = 0; x < 3; x++) {
    for (let y = 2; y >= 0; y--) {
      if(board[x][y] === 'empty' ){
        validMoves.push([x, y])
      }
    }
  }

  return validMoves
}

/**
 *
 * @param board
 * @param simulatedComputerMove
 * @returns {boolean}
 */
function isSafe(board, simulatedComputerMove) {
  //simulate computer move
  let tempComputerBoard = copyBoard(board)

  tempComputerBoard[simulatedComputerMove[0]][simulatedComputerMove[1]] = 'o'

  //if win then exit
  if(hasWinner(tempComputerBoard, 'o') || hasDraw(tempComputerBoard)) {
    return true
  }

  let validHumanMoves = getValidMoves(tempComputerBoard)

  return !validHumanMoves.map((humanMove) => {
    let humanTempBoard = copyBoard(tempComputerBoard)
    //simulate human move
    humanTempBoard[humanMove[0]][humanMove[1]] = 'x'

    //if win then return false
    if (hasWinner(humanTempBoard, 'x')) {
      return false
    }

    if (hasDraw(humanTempBoard)) {
      return true
    }

    const compValidMoves = getValidMoves(humanTempBoard)

    return compValidMoves.map((move) => {
      return isSafe(humanTempBoard, move)
    }).includes(true)
  }).includes(false)
}

/**
 *
 * @param board
 * @returns array[][]
 */
function copyBoard(board) {
  return [[...board[0]], [...board[1]], [...board[2]]]
}


