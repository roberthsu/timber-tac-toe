/**
 *
 * @param board
 * @param player
 * @returns {boolean}
 */
export function hasWinner(board, player) {
  return calculateHorizontalWin(board, player) || calculateVerticalWin(board, player) || calculateDiagonalWin(board, player)
}

/**
 *
 * @param board
 * @returns {boolean}
 */
export function hasDraw(board) {
  for (let x = 0; x < 3; x++) {
    for (let y = 0; y < 3; y++) {
      if(board[x][y] === 'empty')
      {
        return false
      }
    }
  }

  return true
}

export function isGameOver(board) {
  if(hasWinner(board, 'x')) {
    alert('You are the winner!')
    window.location.reload()
  }

  if(hasWinner(board, 'o')) {
    alert('I am the winner!')
    window.location.reload()
  }

  if(hasDraw((board))) {
    alert('Its a draw!')
    window.location.reload()
  }
}

/**
 *
 * @param board
 * @param player
 * @returns {boolean}
 */
function calculateHorizontalWin(board, player) {
  for (let y = 0; y < 3; y++) {
    if(board[0][y] === board[1][y] &&
      board[1][y] === board[2][y] &&
      board[0][y] === player)
    {
      return true
    }
  }
  return false
}

/**
 *
 * @param board
 * @param player
 * @returns {boolean}
 */
function calculateVerticalWin(board, player) {
  for (let x = 0; x < 3; x++) {
    if(board[x][0] === board[x][1] &&
      board[x][1] === board[x][2] &&
      board[x][0] === player)
    {
      return true
    }
  }
  return false
}

/**
 *
 * @param board
 * @param player
 * @returns {boolean}
 */
function calculateDiagonalWin(board, player) {
  if(board[1][1] === player)
  {
    if(
      board[0][0] === board[1][1] &&
      board[1][1] === board[2][2] ||
      board[2][0] === board[1][1] &&
      board[1][1] === board[0][2]
    ) {
      return true
    }
  }
  return false
}
