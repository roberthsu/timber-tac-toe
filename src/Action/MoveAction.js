export default function moveAction(player, square) {
  return {
    type: 'MOVE',
    player: player,
    x: square.xIndex,
    y: square.yIndex
  }
}